
# on-line REFS

## Plagioclase: 
https://geology.com/minerals/plagioclase.shtml
https://en.wikipedia.org/wiki/Anorthite

## mineral densities
https://www.engineeringtoolbox.com/mineral-density-d_1555.html

### Labradorite 
https://cameo.mfa.org/wiki/Labradorite
https://www.webmineral.com/data/Labradorite.shtml
http://gemologyproject.com/wiki/index.php?title=Specific_Gravity
 

# History
2023-04-21 installed phreeqc R package - no problems

